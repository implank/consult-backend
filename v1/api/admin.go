package api

import (
	"consult/global"
	"consult/model"
	"consult/util"
	"errors"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type CreatePostReq struct {
	Title   string `json:"title" binding:"required" validate:"min=1,max=100"`
	Content string `json:"content" binding:"required" validate:"min=1,max=10000"`
}

// GetApplyList
//
//	@Summary		获取申请列表
//	@Description	获取申请列表
//	@Tags			admin
//	@Produce		json
//	@Success		200	{object}	string	"获取成功"
//	@Router			/admin/apply/list [get]
func GetApplyList(c *gin.Context) {
	var applys []model.ExpertApply
	if err := global.DB.Find(&applys).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			c.JSON(200, gin.H{
				"code":    "500",
				"message": "数据库错误",
			})
			return
		}
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "获取成功",
		"data":    applys,
	})
}

// ApproveApply
//
//	@Summary		审批申请
//	@Description	审批申请
//	@Tags			admin
//	@Accept			multipart/form-data
//	@Produce		json
//	@Param			uuid		formData	string	true	"申请uuid"
//	@Param			operation	formData	string	true	"approve or reject"	Enums(approve, reject)
//	@Success		200			{object}	string	"审批成功"
//	@Router			/admin/apply/approve [post]
func ApproveApply(c *gin.Context) {
	uuid := c.PostForm("uuid")
	operation := c.PostForm("operation")
	if operation != "approve" && operation != "reject" {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "operation参数错误",
		})
		return
	}
	var apply model.ExpertApply
	if err := global.DB.Where("uuid = ?", uuid).First(&apply).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			c.JSON(200, gin.H{
				"code":    "400",
				"message": "uuid不存在",
			})
			return
		}
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	if apply.Status != "applying" {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "申请状态不是applying",
		})
		return
	}
	if operation == "reject" {
		if err := global.DB.Model(&apply).Update("status", "rejected").Error; err != nil {
			c.JSON(200, gin.H{
				"code":    "500",
				"message": "数据库错误",
			})
			return
		}
		c.JSON(200, gin.H{
			"code":    "200",
			"message": "拒绝成功",
		})
		return
	}
	expert := model.Expert{
		UUID:     util.NewUUID(),
		Username: apply.Phone,
		Password: apply.Password,
		Phone:    apply.Phone,
	}
	if err := global.DB.Create(&expert).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	expertInfo := model.ExpertInfo{
		ExpertUUID:           expert.UUID,
		Intro:                apply.Intro,
		Avatar:               apply.Avatar,
		Name:                 apply.Name,
		PractcingCertificate: apply.PractcingCertificate,
		ExpertType:           apply.ExpertType,
		AvaliableBefore:      time.Now().Add(time.Duration(apply.ApplyDuration) * 30 * 24 * time.Hour),
	}
	if err := global.DB.Create(&expertInfo).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	if err := global.DB.Model(&apply).Update("status", "approved").Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "审批成功",
	})
}

// CreatePost
//
//	@Summary		创建文章
//	@Description	创建文章
//	@Tags			Post
//	@Accept			json
//	@Produce		json
//	@Param			data	body		CreatePostReq	true	"请求数据"
//	@Success		200		{object}	string			"创建成功"
//	@Router			/posts [post]
func CreatePost(c *gin.Context) {
	var req CreatePostReq
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "参数错误:" + err.Error(),
		})
		return
	}
	post := model.Post{
		UUID:    util.NewUUID(),
		Title:   req.Title,
		Content: req.Content,
	}
	if err := global.DB.Create(&post).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "创建成功",
	})
}

// GetPosts
//
//	@Summary		获取文章列表
//	@Description	获取文章列表
//	@Tags			Post
//	@Produce		json
//	@Success		200	{object}	string	"获取成功"
//	@Router			/posts [get]
func GetPosts(c *gin.Context) {
	var posts []model.Post
	if err := global.DB.Find(&posts).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "获取成功",
		"data":    posts,
	})
}

// GetPost
//
//	@Summary		获取文章
//	@Description	获取文章
//	@Tags			Post
//	@Produce		json
//	@Param			uuid	path		string	true	"文章uuid"
//	@Success		200		{object}	string	"获取成功"
//	@Router			/posts/{uuid} [get]
func GetPost(c *gin.Context) {
	uuid := c.Param("uuid")
	var post model.Post
	if err := global.DB.Where("uuid = ?", uuid).First(&post).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			c.JSON(200, gin.H{
				"code":    "404",
				"message": "id不存在",
			})
			return
		}
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "获取成功",
		"data":    post,
	})
}

// GetContacts
//
//	@Summary		获取联系方式
//	@Description	获取联系方式
//	@Tags			WebInfo
//	@Produce		json
//	@Success		200	{object}	string	"获取成功"
//	@Router			/contacts [get]
func GetContacts(c *gin.Context) {
	var webInfo model.WebInfo
	if err := global.DB.Where("k = ?", "contacts").First(&webInfo).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "获取成功",
		"data": map[string]any{
			"content":    webInfo.V,
			"created_at": webInfo.CreatedAt,
			"updated_at": webInfo.UpdatedAt,
		},
	})
}

// ModifyContacts
//
//	@Summary		修改联系方式
//	@Description	修改联系方式
//	@Tags			WebInfo
//	@Accept			multipart/form-data
//	@Produce		json
//	@Param			contacts	formData	string	true	"联系方式"
//	@Success		200			{object}	string	"修改成功"
//	@Router			/contacts [put]
func ModifyContacts(c *gin.Context) {
	var webInfo model.WebInfo
	if err := global.DB.Where("k = ?", "contacts").First(&webInfo).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	contacts := c.PostForm("contacts")
	webInfo.V = contacts
	webInfo.UpdatedAt = time.Now()
	if err := global.DB.Where("k = ?", "contacts").Updates(&webInfo).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "修改成功",
	})
}

// CreateProduct
//
//	@Summary		创建产品
//	@Description	创建产品
//	@Tags			Product
//	@Accept			multipart/form-data
//	@Produce		json
//	@Param			name		formData	string	true	"产品名称"
//	@Param			description	formData	string	true	"产品描述"
//	@Param			prod_pic		formData	file	true	"产品图片"
//	@Success		200			{object}	string	"创建成功"
//	@Router			/products [post]
func CreateProduct(c *gin.Context) {
	name := c.PostForm("name")
	description := c.PostForm("description")
	prodPic, err := c.FormFile("prod_pic")
	if err != nil {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "avatar参数错误",
		})
		return
	}
	product := model.Product{
		UUID:        util.NewUUID(),
		Name:        name,
		Description: description,
	}
	if err := global.DB.Create(&product).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "创建product数据库错误",
		})
		return
	}
	prodPicMd5, _ := util.Md5(prodPic.Filename + "product" + time.Now().String())
	picFilename := fmt.Sprintf("%d.%s.jpg", time.Now().Unix(), prodPicMd5)
	if err := c.SaveUploadedFile(prodPic, "static/"+picFilename); err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "pic保存错误",
		})
		return
	}
	pic := model.Pic{
		ExternalUUID: product.UUID,
		Link:         picFilename,
	}
	if err := global.DB.Create(&pic).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "创建pic数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "创建成功",
	})
}

// GetProducts
//
//	@Summary		获取产品
//	@Description	获取产品
//	@Tags			Product
//	@Produce		json
//	@Success		200	{object}	string	"获取成功"
//	@Router			/products [get]
func GetProducts(c *gin.Context) {
	var products []model.Product
	if err := global.DB.Find(&products).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "查找prod数据库错误",
		})
		return
	}
	res := make([]map[string]any, len(products))
	for i, product := range products {
		var pic model.Pic
		if err := global.DB.Where("external_uuid = ?", product.UUID).First(&pic).Error; err != nil {
			c.JSON(200, gin.H{
				"code":    "500",
				"message": "查找pic数据库错误",
			})
			return
		}
		res[i] = map[string]any{
			"name":        product.Name,
			"description": product.Description,
			"pic":         pic.Link,
		}
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "获取成功",
		"data":    res,
	})
}
