package api

import (
	"consult/auth"
	"consult/global"
	"consult/model"
	"consult/util"
	"errors"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type LoginReq struct {
	Username string `json:"username"`
	Password string `json:"password"`
	UserType string `json:"user_type" enums:"user,expert"`
}

// Login
//
//	@Summary		登录
//	@Description	登录
//	@Tags			user
//	@Accept			json
//	@Produce		json
//	@Param			loginreq	body		LoginReq	true	"登录请求"
//	@Success		200			{object}	string		"登录成功"
//	@Router			/login [post]
func Login(c *gin.Context) {
	var req LoginReq
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "参数错误",
		})
		fmt.Println(req, err)
		return
	}
	if req.UserType == "user" {
		var user model.User
		if err := global.DB.
			Where("(username = ? or phone = ?) and password = ?", req.Username, req.Username, req.Password).
			First(&user).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				c.JSON(200, gin.H{
					"code":    "400",
					"message": "用户名或密码错误",
				})
				return
			} else {
				c.JSON(200, gin.H{
					"code":    "500",
					"message": "数据库错误",
				})
				return
			}
		}
		if token, err := auth.GenerateAuthToken(user.UUID, "user"); err != nil {
			panic(err)
		} else {
			c.Header("x-token", token)
			c.JSON(200, gin.H{
				"code":     "200",
				"message":  "登录成功",
				"uuid":     user.UUID,
				"role":     user.Role,
				"username": user.Username,
			})
		}
	} else if req.UserType == "expert" {
		var expert model.Expert
		if err := global.DB.
			Where("(username = ? or phone = ?) and password = ?", req.Username, req.Username, req.Password).
			First(&expert).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				c.JSON(200, gin.H{
					"code":    "400",
					"message": "用户名或密码错误",
				})
				return
			} else {
				c.JSON(200, gin.H{
					"code":    "500",
					"message": "数据库错误",
				})
				return
			}
		}
		if token, err := auth.GenerateAuthToken(expert.UUID, "expert"); err != nil {
			panic(err)
		} else {
			c.Header("x-token", token)
			c.JSON(200, gin.H{
				"code":     "200",
				"message":  "登录成功",
				"uuid":     expert.UUID,
				"username": expert.Username,
			})
		}
	} else {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "类型错误",
		})
		return
	}
}

// SendCaptcha
//
//	@Summary		发送验证码
//	@Description	发送验证码
//	@Tags			user
//	@Accept			x-www-form-urlencoded
//	@Produce		json
//	@Param			phone	formData	string	true	"手机号"
//	@Param			method	formData	string	true	"方法"	Enums(user-register,expert-register)
//	@Success		200		{object}	string	"发送成功"
//	@Router			/send_captcha [post]
func SendCaptcha(c *gin.Context) {
	phone := c.PostForm("phone")
	method := c.PostForm("method")
	if len(phone) < 6 {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "手机号太短啦！",
		})
		return
	}
	if method == "user-register" {
		var user model.User
		if err := global.DB.Where("phone = ?", phone).First(&user).Error; err != nil {
			if !errors.Is(err, gorm.ErrRecordNotFound) {
				c.JSON(200, gin.H{
					"code":    "500",
					"message": "数据库错误",
				})
				return
			}
			goto send_captcha
		} else {
			c.JSON(200, gin.H{
				"code":    "400",
				"message": "手机号已注册",
			})
			return
		}
	} else if method == "expert-register" {
		var expert model.Expert
		if err := global.DB.Where("phone = ?", phone).First(&expert).Error; err != nil {
			if !errors.Is(err, gorm.ErrRecordNotFound) {
				c.JSON(200, gin.H{
					"code":    "500",
					"message": "数据库错误",
				})
				return
			}
			goto send_captcha
		} else {
			c.JSON(200, gin.H{
				"code":    "400",
				"message": "手机号已注册",
			})
			return
		}
	} else {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "method参数错误",
		})
		return
	}
send_captcha:
	//do send
	global.RD.Set(fmt.Sprintf("%s-captcha-%s", method, phone), phone[:6], 5*time.Minute)
	c.JSON(200, gin.H{
		"code":         "200",
		"message":      "发送成功",
		"fake-captcha": phone[:6],
	})
}

// UserRegister
//
//	@Summary		普通用户注册
//	@Description	普通用户注册
//	@Tags			user
//	@Accept			x-www-form-urlencoded
//	@Produce		json
//	@Param			username	formData	string	true	"用户名"
//	@Param			password	formData	string	true	"密码"
//	@Param			phone		formData	string	true	"手机号"
//	@Param			captcha		formData	string	true	"验证码"
//	@Success		200			{object}	string	"注册成功"
//	@Router			/user_register [post]
func UserRegister(c *gin.Context) {
	username := c.PostForm("username")
	password := c.PostForm("password")
	phone := c.PostForm("phone")
	captcha := c.PostForm("captcha")
	if username == "" || password == "" || phone == "" || captcha == "" {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "参数不能为空",
		})
		return
	}
	if len(phone) < 6 {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "手机号太短啦！",
		})
		return
	}
	if v, err := global.RD.Get(
		fmt.Sprintf("user-register-captcha-%s", phone)).
		Result(); err != nil || v != captcha {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "验证码错误",
		})
		return
	}
	newUser := model.User{
		UUID:     util.NewUUID(),
		Username: username,
		Password: password,
		Phone:    phone,
		Role:     "user",
	}
	if err := global.DB.Create(&newUser).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "注册成功",
	})
}
