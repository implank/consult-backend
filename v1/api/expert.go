package api

import (
	"consult/global"
	"consult/model"
	"consult/util"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ChatReq struct {
	SenderType string `json:"senderType" enums:"user,expert"`
	SenderID   string `json:"senderID"`
	ReceiverID string `json:"receiverID"`
	Content    string `json:"content"`
}

// GetExpertList
//
//	@Summary		获取专家列表
//	@Description	获取专家列表
//	@Tags			expert
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	string	"获取成功"
//	@Router			/expert/list [get]
func GetExpertList(c *gin.Context) {
	var expertinfos []model.ExpertInfo
	if err := global.DB.Find(&expertinfos).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "获取成功",
		"data":    expertinfos,
	})
}

// GetExperts
//
//	@Summary		获取专家
//	@Description	获取专家
//	@Tags			expert
//	@Produce		json
//	@Param			expert_type	query	string	true	"专家类型"	Enums(layer, company)
//	@Success		200			{object}	string	"获取成功"
//	@Router			/experts [get]
func GetExperts(c *gin.Context) {
	expert_type := c.Query("expert_type")
	var experts []model.ExpertInfo
	if err := global.DB.Where("expert_type = ?", expert_type).Find(&experts).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "查询expert数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "获取成功",
		"data":    experts,
	})
}

// Chat
//
//	@Summary		聊天
//	@Description	聊天
//	@Tags			expert
//	@Accept			json
//	@Produce		json
//	@Param			chatreq	body		ChatReq	true	"聊天请求"
//	@Success		200		{object}	string	"消息创建成功"
//	@Router			/chat [post]
func Chat(c *gin.Context) {
	var req ChatReq
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "参数错误",
		})
		return
	}
	chatMessage := model.ChatMessage{
		SenderType: req.SenderType,
		SenderID:   req.SenderID,
		ReceiverID: req.ReceiverID,
		Content:    req.Content,
		CreatedAt:  time.Now(),
	}
	if err := global.DB.Create(&chatMessage).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	// chatStatus := model.ChatStatus{
	// 	SenderID:   req.SenderID,
	// 	ReceiverID: req.ReceiverID,
	// 	Status:     "unread",
	// 	CreatedAt:  time.Now(),
	// 	UpdatedAt:  time.Now(),
	// }
	var chatStatus model.ChatStatus
	if err := global.DB.
		Where("sender_id = ? and receiver_id = ?", req.SenderID, req.ReceiverID).
		First(&chatStatus).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			chatStatus = model.ChatStatus{
				SenderID:   req.SenderID,
				ReceiverID: req.ReceiverID,
				Status:     "unread",
				CreatedAt:  time.Now(),
				UpdatedAt:  time.Now(),
			}
			global.DB.Create(&chatStatus)
		}
	} else {
		chatStatus.Status = "unread"
		chatStatus.UpdatedAt = time.Now()
		global.DB.Save(&chatStatus)
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "消息创建成功",
	})
}

// ChatHistory
//
//	@Summary		聊天历史
//	@Description	聊天历史
//	@Tags			expert
//	@Param			sender		query		string	true	"发送者"
//	@Param			receiver	query		string	true	"接收者"
//	@Success		200			{object}	string	"获取消息记录成功"
//	@Router			/chat/history [GET]
func ChatHistory(c *gin.Context) {
	sender := c.Query("sender")
	receiver := c.Query("receiver")
	var chatMessages []model.ChatMessage
	if err := global.DB.Where("(sender_id = ? and receiver_id = ?) or (sender_id = ? and receiver_id = ?)", sender, receiver, receiver, sender).
		Order("created_at desc").
		Find(&chatMessages).Error; err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "获取消息记录成功",
		"data":    chatMessages,
	})
}

// ExpertApply
//
//	@Summary		专家申请
//	@Description	专家申请
//	@Tags			expert
//	@Accept			multipart/form-data
//	@Produce		json
//	@Param			password				formData	string	true	"密码"
//	@Param			phone					formData	string	true	"手机号"
//	@Param			captcha					formData	string	true	"验证码"
//	@Param			practcingCertificate	formData	file	true	"执业证书"
//	@Param			avatar					formData	file	true	"头像"
//	@Param			name					formData	string	true	"姓名"
//	@Param			intro					formData	string	true	"简介"
//	@Param			applyDuration			formData	integer	true	"申请时长"
//	@Param			expert_type				formData	string	true	"类型"	Enums(layer, company)	default(layer)
//	@Success		200						{object}	string	"创建申请成功"
//	@Router			/expert/apply [post]
func ExpertApply(c *gin.Context) {
	password := c.PostForm("password")
	phone := c.PostForm("phone")
	captcha := c.PostForm("captcha")
	expertType := c.PostForm("expert_type")
	practcingCertificate, err := c.FormFile("practcingCertificate")
	if err != nil {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "practcingCertificate参数错误",
		})
		return
	}
	avatar, err := c.FormFile("avatar")
	if err != nil {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "avatar参数错误",
		})
		return
	}
	name := c.PostForm("name")
	intro := c.PostForm("intro")
	applyDurationStr := c.PostForm("applyDuration")
	applyDuration, err := strconv.ParseUint(applyDurationStr, 10, 64)
	if err != nil {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "applyDuration参数错误",
		})
		return
	}
	// check captcha
	if captcha != global.RD.Get("expert-register-captcha-"+phone).Val() {
		c.JSON(200, gin.H{
			"code":    "400",
			"message": "验证码错误",
		})
		return
	}
	// store avatar and practcingCertificate
	pCMd5, err := util.Md5(practcingCertificate.Filename + "practcingCertificate" + phone + time.Now().String())
	if err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "pCMd5错误",
		})
		return
	}
	avatarMd5, err := util.Md5(avatar.Filename + "avatar" + phone + time.Now().String())
	if err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "avatarMd5错误",
		})
		return
	}
	pcFilename := fmt.Sprintf("%d.%s.jpg", time.Now().Unix(), pCMd5)
	avatarFilename := fmt.Sprintf("%d.%s.jpg", time.Now().Unix(), avatarMd5)
	if err := c.SaveUploadedFile(practcingCertificate, "static/avatar/"+pcFilename); err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "practcingCertificate保存错误",
		})
		return
	}
	if err := c.SaveUploadedFile(avatar, "static/avatar/"+avatarFilename); err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "avatar保存错误",
		})
		return
	}
	expertApply := model.ExpertApply{
		UUID:                 util.NewUUID(),
		Password:             password,
		Phone:                phone,
		PractcingCertificate: pcFilename,
		Avatar:               avatarFilename,
		Name:                 name,
		Intro:                intro,
		Status:               "applying",
		ApplyDuration:        applyDuration,
		ExpertType:           expertType,
		CreatedAt:            time.Now(),
	}
	if err := global.DB.Create(&expertApply).Error; err != nil {
		c.JSON(200, gin.H{
			"code":    "500",
			"message": "创建expertApply数据库错误",
		})
		return
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "创建申请成功",
	})
}

// HistoryChat
//
//	@Summary		历史聊天记录
//	@Description	历史聊天记录
//	@Tags			expert
//	@Param			uuid	query		string	true	"用户uuid"
//	@Success		200		{object}	string	"获取消息记录成功"
//	@Router			/history_chat [get]
func HistoryChat(c *gin.Context) {
	uuid := c.Query("uuid")
	var chatStatus []model.ChatStatus
	global.DB.Where("receiver_id = ?", uuid).Find(&chatStatus)
	ret := make([]gin.H, 0)
	for _, v := range chatStatus {
		var user model.User
		global.DB.Where("uuid = ?", v.SenderID).First(&user)
		ret = append(ret, gin.H{
			"senderName": user.Username,
			"senderID":   v.SenderID,
			"receiverID": v.ReceiverID,
			"status":     v.Status,
			"createdAt":  v.CreatedAt,
			"updatedAt":  v.UpdatedAt,
		})
	}
	c.JSON(200, gin.H{
		"code":    "200",
		"message": "获取消息记录成功",
		"data":    ret,
	})
}
