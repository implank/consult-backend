package util

import (
	"crypto/md5"
	"encoding/hex"
	"math/rand"
	"strconv"

	"github.com/bwmarrin/snowflake"
)

var node *snowflake.Node

// return 36-base uuid
func NewUUID() string {
	if node == nil {
		node, _ = snowflake.NewNode(rand.Int63n(1023))
	}
	uuid := node.Generate()
	return strconv.FormatInt(int64(uuid), 36)
}

func Md5(s string) (string, error) {
	f := md5.New()
	_, err := f.Write([]byte(s))
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(f.Sum(nil)), nil
}
