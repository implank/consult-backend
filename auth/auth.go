package auth

import (
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/spf13/viper"
)

type AuthClaims struct {
	UserUUID string `json:"uuid"`
	UserType string `json:"user_type"`
	jwt.StandardClaims
}

func GenerateAuthToken(uuid string, userType string) (signedToken string, err error) {
	claims := AuthClaims{
		UserUUID: uuid,
		UserType: userType,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
			IssuedAt:  time.Now().Unix(),
			Issuer:    "consult",
			Subject:   "auth",
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	secret := viper.GetString("jwt.secret")
	signedToken, err = token.SignedString([]byte(secret))
	if err != nil {
		return
	}
	return
}

func ParseAuthToken(signedToken string) (claims *AuthClaims, err error) {
	secret := viper.GetString("jwt.secret")
	token, err := jwt.ParseWithClaims(
		signedToken,
		claims,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(secret), nil
		},
	)
	if err != nil || !token.Valid {
		return
	}
	return
}
