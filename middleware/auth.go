package middleware

import (
	"consult/auth"

	"github.com/gin-gonic/gin"
)

func Auth(c *gin.Context) {
	jwt_token := c.GetHeader("x-token")
	claims, err := auth.ParseAuthToken(jwt_token)
	if err != nil {
		c.JSON(200, gin.H{
			"code":    "401",
			"message": "未登录",
		})
		c.Abort()
		return
	}
	// todo block user
	c.Set("user_uuid", claims.UserUUID)
	c.Set("user_type", claims.UserType)
	c.Next()
}
