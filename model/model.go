package model

import (
	"time"
)

type User struct {
	ID       int    `json:"id" gorm:"primary_key"`
	UUID     string `json:"uuid"`
	Username string `json:"username"`
	Password string `json:"password"`
	Phone    string `json:"phone"`
	Role     string `json:"role"`
}

type Expert struct {
	ID       int    `json:"id" gorm:"primary_key"`
	UUID     string `json:"uuid"`
	Username string `json:"username" comment:"登录时用的"`
	Password string `json:"password"`
	Phone    string `json:"phone"`
}

type ExpertInfo struct {
	ID                   int    `json:"id" gorm:"primary_key"`
	ExpertUUID           string `json:"expertUUID"`
	Intro                string `json:"intro" comment:"专家简介"`
	Avatar               string `json:"avatar" comment:"头像token,用于获取头像"`
	Name                 string `json:"name"`
	PractcingCertificate string `json:"practcingCertificate"`
	ExpertType           string `json:"expert_type" enums:"layer, company" gorm:"default:layer"`

	AvaliableBefore time.Time `json:"avaliableBefore"`
}

type ExpertApply struct {
	ID                   int    `json:"id" gorm:"primary_key"`
	UUID                 string `json:"uuid"`
	Password             string `json:"password"`
	Phone                string `json:"phone"`
	PractcingCertificate string `json:"practcingCertificate"`
	Avatar               string `json:"avatar" comment:"头像token"`
	Name                 string `json:"name"`
	Intro                string `json:"intro" comment:"专家简介"`
	Status               string `json:"status" enums:"rejected,approved,applying"`
	ApplyDuration        uint64 `json:"applyDuration" comment:"申请时长，单位月"`
	ExpertType           string `json:"expert_type" enums:"layer, company" gorm:"default:layer"`

	CreatedAt time.Time `json:"createdAt"`
}

type ChatMessage struct {
	ID         int    `json:"id" gorm:"primary_key"`
	SenderType string `json:"senderType" enums:"user,expert"`
	SenderID   string `json:"senderID"`
	ReceiverID string `json:"receiverID"`
	Content    string `json:"content"`

	CreatedAt time.Time `json:"createdAt"`
}

type ChatStatus struct {
	ID         int    `json:"id" gorm:"primary_key"`
	SenderID   string `json:"senderID"`
	ReceiverID string `json:"receiverID"`
	Status     string `json:"status" enums:"unread,read"`

	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

type Post struct {
	ID      int    `json:"id" gorm:"primary_key"`
	UUID    string `json:"uuid"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

type Product struct {
	ID          int    `json:"id" gorm:"primary_key"`
	UUID        string `json:"uuid"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type Pic struct {
	ExternalUUID string `json:"external_uuid"`
	Link         string `json:"link"`
}

type WebInfo struct {
	K         string    `json:"k"`
	V         string    `json:"v"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
