package global

import (
	"github.com/go-redis/redis"
	"github.com/spf13/viper"
	"gorm.io/gorm"
)

var (
	DB *gorm.DB
	RD *redis.Client
	VP *viper.Viper
)
