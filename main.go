package main

import (
	"consult/global"
	"consult/middleware"
	"consult/model"
	"consult/v1/api"
	"fmt"

	_ "consult/docs"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/spf13/viper"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func initViper() {
	viper.SetConfigFile("config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}
}

func initDB() {
	// init mysql
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		viper.GetString("db.username"),
		viper.GetString("db.password"),
		viper.GetString("db.host"),
		viper.GetString("db.port"),
		viper.GetString("db.schema"),
	)
	if db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{}); err != nil {
		panic(err)
	} else {
		global.DB = db
	}
	global.DB.AutoMigrate(model.User{})
	global.DB.AutoMigrate(model.Expert{})
	global.DB.AutoMigrate(model.ExpertInfo{})
	global.DB.AutoMigrate(model.ExpertApply{})

	global.DB.AutoMigrate(model.ChatMessage{})
	global.DB.AutoMigrate(model.ChatStatus{})
	global.DB.AutoMigrate(model.Post{})
	global.DB.AutoMigrate(model.Product{})
	global.DB.AutoMigrate(model.Pic{})
	global.DB.AutoMigrate(model.WebInfo{})

	// init redis
	rd := redis.NewClient(&redis.Options{
		Addr: viper.GetString("redis.addr"),
	})
	if _, err := rd.Ping().Result(); err != nil {
		panic(err)
	} else {
		global.RD = rd
	}
}

func initStatic() {
}

func initRouter(r *gin.Engine) {
	r.Use(middleware.Cors)

	r.GET("/", func(c *gin.Context) { c.JSON(200, gin.H{"message": "index"}) })
	r.POST("/", func(c *gin.Context) { c.JSON(200, gin.H{"message": "post index"}) })
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	r.Static("/static", "./static")

	p := r.Group("/api")
	p.POST("/login", api.Login)
	p.POST("/send_captcha", api.SendCaptcha)
	p.POST("/user_register", api.UserRegister)
	// r.POST("/expert_register", api.ExpertRegister)

	p.GET("/expert/list", api.GetExpertList)
	p.GET("/experts", api.GetExperts)
	p.POST("/chat", api.Chat)
	p.GET("/chat/histroy", api.ChatHistory)
	p.GET("/chat/history", api.ChatHistory)
	p.POST("/expert/apply", api.ExpertApply)
	p.GET("/history_chat", api.HistoryChat)

	p.GET("/admin/apply/list", api.GetApplyList)
	p.POST("/admin/apply/approve", api.ApproveApply)

	p.GET("/posts", api.GetPosts)
	p.POST("/posts", api.CreatePost)
	p.GET("/posts/:uuid", api.GetPost)

	p.GET("contacts", api.GetContacts)
	p.PUT("contacts", api.ModifyContacts)

	p.GET("products", api.GetProducts)
	p.POST("products", api.CreateProduct)
	// p.GET("products/:uuid", api.GetProduct)
}

// @title		consult API
// @version	1.0
// @BasePath	/api
// @schemes	http
func main() {
	initViper()
	initDB()
	initStatic()
	r := gin.Default()
	initRouter(r)
	if err := r.Run(":" + viper.GetString("serve_port")); err != nil {
		panic(err)
	}
}
